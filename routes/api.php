<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/***this are neccessary routes for testing purposes only***/
// Route::post('/register','UserController@register');
// Route::post('/login','UserController@login');

// Route::post('/saveDetails','UserController@saveDetails');

// Route::post('/saveWarehouse','WarehouseController@insert');

// Route::post('/saveProducts','ProductController@insert');

// Route::post('/saveDelivery','DeliveryController@insert');
// Route::get('/token/{id}','TokenController@generate');
/***this are neccessary routes for testing purposes only***/

Route::post('/searchroutes','TrackerController@search');

Route::middleware('checklogin')->group(function () {
	Route::post('/addTracker','TrackerController@insert');
	Route::post('/addRoutes','RouteController@insert');

	Route::put('/updateTracker/{id}','TrackerController@update');
	Route::put('/updateRoute/{id}','RouteController@update');

	Route::delete('/deleteTracker/{id}','TrackerController@delete');
	Route::delete('/deleteRoute/{id}','RouteController@delete');
});
