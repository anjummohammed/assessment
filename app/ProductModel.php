<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductModel extends Model
{
	public static function checkWarehouse($id) {
    	$sql = '';
    	try {
			$sql = DB::table('warehouses')
    			  ->select('id')
    			  ->where('id','=',$id)
    			  ->get();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function checkProduct($id) {
        $sql = '';
        try {
            $sql = DB::table('products')
                  ->select('id')
                  ->where('id','=',$id)
                  ->get();
        } catch (Exception $e) {

        } finally {
            return $sql;
        }
    }

     public static function createProduct($warehouse_id,$name,$description,$qty,$price) {
    	$sql = '';
    	try {
			$sql = DB::table('products')->insert([
			    [
			     'warehouse_id' => $warehouse_id, 
			     'name' => $warehouse_id,
			     'description' => $description,
			     'qty' => $qty,
			     'price' => $price
			    ]
			]);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }
}
