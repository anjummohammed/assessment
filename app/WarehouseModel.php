<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WarehouseModel extends Model
{
    public static function createWarehouse($postalcode,$country,$state,$city,$address,$name) {
    	$sql = '';
    	try {
			$sql = DB::table('warehouses')->insert([
			    [
			     'name' => $name, 
			     'address' => $address,
			     'city' => $city,
			     'state' => $state,
			     'country' => $country,
			     'postalcode' => $postalcode
			    ]
			]);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }
}
