<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TokenModel extends Model
{
    public static function generateToken($id,$token){
    	$sql = DB::table('tokens')->insert([
		    ['token' => $token, 'user_id' => $id]
		]);
    	return $sql;
    }

    public static function getToken($token){
    	$sql = DB::table('tokens')
    			  ->join('users_details','users_details.user_id','=','tokens.user_id')
    			  ->select('users_details.role_id','tokens.token')
    			  ->where('tokens.token','=',$token)
    			  ->where('tokens.is_active','=',1)
    			  ->get();
    	return $sql;
    }
}
