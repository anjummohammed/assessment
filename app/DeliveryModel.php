<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DeliveryModel extends Model
{
    public static function createDelivery($product_id,$user_id,$status) {
    	$sql = '';
    	try {
			$sql = DB::table('delivery_details')->insert([
			    [
			     'product_id' => $product_id, 
			     'user_id' => $user_id,
			     'status' => $status
			    ]
			]);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }
}
