<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TrackerModel extends Model
{
	
    public static function createTracker($start_route,$origin,$destination) {
    	$sql = '';
    	try {
			$sql = DB::table('trackers')->insert([
			    [
			     'start_route' => $start_route,
			     'origin' => $origin,
			     'destination' => $destination
			    ]
			]);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function searchTracker($id) {
    	$sql = '';
    	try {
			$sql = DB::table('trackers')
    			  ->select('id','start_route','origin','destination')
    			  ->where('id','=',$id)
    			  ->get();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function searchTrackerByOriginAndDestination($origin,$destination) {
    	$sql = '';
    	try {
			$sql = DB::table('trackers')
    			  ->select('id','start_route','origin','destination')
    			  ->where('origin','=',$origin)
    			  ->where('destination','=',$destination)
    			  ->get();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function updateTracker($id,$fields) {
    	$sql = '';
    	try {
			$sql = DB::table('trackers')
    			  ->where('id',$id)
    			  ->update($fields);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function deleteTracker($id) {
    	$sql = '';
    	try {
  			$sql = DB::table('trackers')
    			  ->where('id',$id)
    			  ->delete();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }
}
