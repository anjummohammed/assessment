<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserModel extends Model
{

    public static function createUser($name,$email,$password){
    	$sql = DB::table('users')->insert([
		    ['name' => $name, 'email' => $email,'password' => $password]
		]);
    	return $sql;
    }

    public static function checkUser($id) {
    	$sql = '';
    	try {
			$sql = DB::table('users')
    			  ->select('id')
    			  ->where('id','=',$id)
    			  ->get();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function saveDeliveryDetails($postalcode,$country,$state,$city,$address,$user_id,$role) {
    	$sql = '';
    	try {
			$sql = DB::table('users_details')->insert([
			    [
			     'user_id' => $user_id, 
			     'address' => $address,
			     'city' => $city,
			     'state' => $state,
			     'country' => $country,
			     'postalcode' => $postalcode,
                 'role_id' => $role
			    ]
			]);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }
}
