<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\WarehouseModel;

class WarehouseController extends Controller
{
    public function insert(Request $request) {
    	$isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('name','address','city','state','country','postalcode');

            $rules = array(
            			   'postalcode'=>['required','regex:/^[a-zA-Z0-9]/'],
            			   'country'=>['required','regex:/^[a-zA-Z]/'],
                           'state'=>['required','regex:/^[a-zA-Z]/'],
                           'city'=>['required','regex:/^[a-zA-Z]/'],
                           'address'=>['required','regex:/^[a-zA-Z0-9]/'],
                           'name'=>['required','regex:/^[a-zA-Z]/']
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {

                $postalcode = $input['postalcode'];
                $country = $input['country'];
                $state = $input['state'];
                $city = $input['city'];
                $address = $input['address'];
                $name = $input['name'];
                $isInserted = WarehouseModel::createWarehouse($postalcode,$country,$state,$city,$address,$name);
                //$isInserted = true;
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,201);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }
}
