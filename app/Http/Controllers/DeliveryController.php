<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\DeliveryModel;
use App\ProductModel;
use App\UserModel;

class DeliveryController extends Controller
{
    public function checkProduct($id) {
    	$bool = '';
    	try {
    		$isFound = ProductModel::checkProduct($id);
    		if (sizeof($isFound) > 0) {
    			$bool = true;
    		} else {
    			$bool = false;
    		}
    	} catch (Exception $e) {
    		
    	} finally {
    		return $bool;
    	}
    }

    public function checkUser($id) {
    	$bool = '';
    	try {
    		$isFound = UserModel::checkUser($id);
    		if (sizeof($isFound) > 0) {
    			$bool = true;
    		} else {
    			$bool = false;
    		}
    	} catch (Exception $e) {
    		
    	} finally {
    		return $bool;
    	}
    }

    public function insert(Request $request) {
    	$isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('user_id','product_id','status');

            $rules = array(
                           'product_id'=>['required','regex:/^[0-9]+$/'],
                           'user_id'=>['required','regex:/^[0-9]+$/']
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {

                $product_id = $input['product_id'];
                $user_id = $input['user_id'];

                if($this->checkUser($user_id) && $this->checkProduct($product_id)) {
                	$status = 'Pending';
                	$isInserted = DeliveryModel::createDelivery($product_id,$user_id,$status);
                } else {
                	$error['errors'] = 'Results not found';
                }
                
                //$isInserted = true;
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,201);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }
}
