<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TokenModel;

class TokenController extends Controller
{
    public function generate($user_id){
    	$isInserted = false;
        $token = '';
    	try {
    		$token = md5(microtime().''.$user_id);
    		$isInserted = TokenModel::generateToken($user_id,$token);
    	} catch(Exception $error) {

    	} finally {
            $json = '';
    		if ($isInserted) {
                $json = $token;
            }
            return $json;
    	}
    }
}
