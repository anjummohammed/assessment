<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\TrackerModel;
use App\RoutesModel;
use App\TokenModel;

class TrackerController extends Controller
{
	private $token = '';

	public function __construct() {
		$header = explode(' ',Request()->header('Authorization'));
		$this->token = $header[1];
	}

    public function searchById($id) {
    	$data = '';
        try {
        	$data = TrackerModel::searchTracker($id);
        } catch(Exception $error) {

        } finally {
        	$reponse = '';
        	if (sizeof($data) > 0) {
        		$response = response()->json($data,200);
        	} else {
        		$response = response()->json(array('error' => 'Not Found'),404);
        	}
            return $response;
        }
    }

    public function search(Request $request) {
    	$isvalid = false;
        $error['errors'] = [];
        $routesArr = [];
        $pathsArr = [];

        try {
            $input = $request->only('origin','destination');

            $rules = array(
                           'origin'=>['required','regex:/^[a-zA-Z0-9]+$/'],
                           'destination'=>['required','regex:/^[a-zA-Z0-9]+$/'],
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isvalid = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {

                $origin = $input['origin'];
                $destination = $input['destination'];
                $isFound = TrackerModel::searchTrackerByOriginAndDestination($origin,$destination);
                $data = json_decode($isFound,true);

                $routes = RoutesModel::getAllRoutes();
                $dataRoutes = json_decode($routes,true);
                
                foreach ($data as $key => $value) {
                    $keyRoute = array_keys(array_column($dataRoutes, 'tracker_id'),$value['id']);
                    $arrRoutes = [];
                    foreach ($keyRoute as $Routevalue) {
                        array_push($arrRoutes, $dataRoutes[$Routevalue]);
                    }
                    $value['routes'] = $arrRoutes;
                    $value['count_routes'] = sizeof($arrRoutes);
                    $value['total_time'] = array_sum(array_column($arrRoutes, 'time'));
                    $value['total_cost'] = array_sum(array_column($arrRoutes, 'cost'));
                    if (sizeof($arrRoutes) > 0 ) {
                        array_push($routesArr,$value);
                    }
                }

                $arrTrackerId = [];

                foreach ($routesArr as $key => $value) {
                    $arrKeys = array_keys(array_column($routesArr[$key]['routes'], 'to_point'),'B');
                    foreach ($arrKeys as $arrKey => $arrValue) {
                        $tracker_id = $routesArr[$key]['routes'][$arrValue];
                        array_push($arrTrackerId,$tracker_id['tracker_id']);
                    }
                }
                
                foreach ($arrTrackerId as $key => $value) {
                    $arrKeys = array_keys(array_column($routesArr, 'id'),$value);
                    foreach ($arrKeys as $arrKey => $arrValue) {
                        $routesData = $routesArr[$arrValue];
                        array_push($pathsArr,$routesData);
                    }
                }

                $isvalid = true;
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isvalid) {

                $finalRoute = [];
                $searchIndex = 0;

                foreach ($pathsArr as $key => $value) {
                    $sum = $value['total_cost'] + $value['total_time'];
                    $r = array('tracker_id' => $value['id'],'total' => $sum);
                    array_push($finalRoute,$r);
                }
                
                $searchItem = min(array_column($finalRoute, 'total'));
                $id = array_keys(array_column($finalRoute, 'total'),$searchItem);
                $tracker_id = $finalRoute[$id[0]]['tracker_id'];
                $tracker_id = array_keys(array_column($pathsArr, 'id'),$tracker_id); 
                $shortestRoute = $pathsArr[$tracker_id[0]];

                $shortest_path = [];

                foreach ($shortestRoute['routes'] as $key => $value) {
                    if(!in_array($value['from_point'],$shortest_path)) {
                        array_push($shortest_path,$value['from_point']);
                    }

                    if(!in_array($value['to_point'],$shortest_path)) {
                        array_push($shortest_path,$value['to_point']);
                    }
                }

                $shortest_path = implode('-', $shortest_path);

                $shortestRoute['best_route'] = $shortest_path;

            	if (sizeof($isFound) > 0) {
            		$json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data found',
                              'data' => $shortestRoute
                              );
            	} else {
            		$json = array('errors'=>[],
                              'status' => 'Not Found',
                              'code' => 404,
                              'msg' => 'data not found'
                              );
            	}
                
                $responseJSON = response()->json($json,200);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }

    public function delete($id) {
    	$response = '';
    	$result = '';

        try {
        	
            $isFoundRoute = RoutesModel::searchRoute(NULL,NULL,NULL,$id);

            if (sizeof($isFoundRoute) > 0) {
                $response = response()->json(array('message' => 'There are records on trackers table'),200);
            } else {
                $isFound = TrackerModel::searchTracker($id);

                if (sizeof($isFound) > 0) {
                    $result = TrackerModel::deleteTracker($id); 
                } else {
                    $response = response()->json(array('error' => 'Not Found'),404);
                }
            }
        	
        } catch(Exception $error) {

        } finally {

        	if ($result) {
        		$response = response()->json(array('message' => 'Tracker Deleted'),200);
        	}

            return $response;
        }

    }

    public function update(Request $request,$id) {
    	$response = '';
    	$result = '';

        try {

        	$isFound = TrackerModel::searchTracker($id);
        	
        	if (sizeof($isFound) > 0) {
        		$fields = $request->only('start_route','origin','destination');
	        	$result = TrackerModel::updateTracker($id,$fields);	
        	} else {
        		$response = response()->json(array('error' => 'Not Found'),404);
        	}

        } catch(Exception $error) {

        } finally {

        	if ($result) {
        		$response = response()->json(array('message' => 'Tracker Updated'),200);
        	} else {
        		$response = response()->json(array('message' => 'An Error Occured'),500);
        	}

            return $response;
        }

    }

    public function insert(Request $request) {
    	$isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('start_route','origin','destination');

            $rules = array(
            			   'start_route'=>['required','regex:/^[a-zA-Z0-9 .\-]+$/'],
                           'origin'=>['required','regex:/^[a-zA-Z0-9 .\-]+$/'],
                           'destination'=>['required','regex:/^[a-zA-Z0-9 .\-]+$/']
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {
            	$start_route = $input['start_route'];
                $origin = $input['origin'];
                $destination = $input['destination'];
                $isInserted = TrackerModel::createTracker($start_route,$origin,$destination);
                $isInserted = true;
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,201);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }
}
