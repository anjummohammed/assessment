<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UserModel;
use App\TokenModel;

class UserController extends Controller
{

    public function login(Request $request){
        $token = '';
        try {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                $user_id = Auth::id();
                $token = $this->generate($user_id);
            } 
        } catch(Exception $error) {

        } finally {
            $json = '';
            if (strlen($token) !== 0) {
                $json = array('code' => 200,'status' => 'ok','token' => $token);
                $responseJSON = response()->json($json,200);
            } else {
                $json = array('code' => 401,'status' => 'Unauthorize Access');
                $responseJSON = response()->json($json,401);
            }
            return $responseJSON;
        }
    }

    public function generate($user_id){
        $isInserted = false;
        $token = '';
        try {
            $token = md5(microtime().''.$user_id);
            $isInserted = TokenModel::generateToken($user_id,$token);
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = $token;
            }
            return $json;
        }
    }

	public function register(Request $request){
        $isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('email','name','password');

            $rules = array('email'=>['required',
                                     'regex:/^[a-zA-Z0-9]/',
                                     'regex:/[@][a-z]/',
                                     'regex:/[.][a-z]/',
                                     'unique:users'
                                    ],
                           'name'=>['required',
                                    'unique:users',
                                    'regex:/^[a-zA-Z0-9]/'],
                           'password'=>'required'
                           );

            $validate = Validator::make($input,$rules);
            
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {
                $email = $input['email'];
                $name = $input['name'];
                $password = bcrypt($input['password']);
                $isInserted = UserModel::createUser($name,$email,$password);
                $isInserted = true;
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,200);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }

    public function checkUser($id) {
    	$bool = '';
    	try {
    		$isFound = UserModel::checkUser($id);
    		if (sizeof($isFound) > 0) {
    			$bool = true;
    		} else {
    			$bool = false;
    		}
    	} catch (Exception $e) {
    		
    	} finally {
    		return $bool;
    	}
    }
	/*
		Save Buyers Details
	*/
    public function saveDetails(Request $request) {
    	$isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('user_id','address','city','state','country','postalcode');

            $rules = array(
            			   'postalcode'=>['required','regex:/^[a-zA-Z0-9]/'],
            			   'country'=>['required','regex:/^[a-zA-Z]/'],
                           'state'=>['required','regex:/^[a-zA-Z]/'],
                           'city'=>['required','regex:/^[a-zA-Z]/'],
                           'address'=>['required','regex:/^[a-zA-Z0-9]/'],
                           'user_id'=>['required','regex:/^[0-9]+$/','unique:users_details']
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {

                $postalcode = $input['postalcode'];
                $country = $input['country'];
                $state = $input['state'];
                $city = $input['city'];
                $address = $input['address'];
                $user_id = $input['user_id'];
                $role = 2;
                if ($this->checkUser($user_id)) {
                	$isInserted = UserModel::saveDeliveryDetails($postalcode,$country,$state,$city,$address,$user_id,$role);
                	$isInserted = true;
                } else {
                	$error['errors'] = 'User not found';
                }
          
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,201);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }
}
