<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\RoutesModel;
use App\TrackerModel;

class RouteController extends Controller
{
    private $token = '';

	public function __construct() {
		$header = explode(' ',Request()->header('Authorization'));
		if(isset($header[1])) {
			$this->token = $header[1];
		} 
	}

	public function insert(Request $request) {
    	$isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('tracker_id','from_point','to_point','cost','time');

            $rules = array(
            			   'tracker_id'=>['required','regex:/^[0-9]+$/'],
                           'from_point'=>['required','regex:/^[a-zA-Z0-9 .\-]+$/'],
                           'to_point'=>['required','regex:/^[a-zA-Z0-9 .\-]+$/'],
                           'cost'=>['required','regex:/^[0-9]+$/'],
                           'time'=>['required']
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {

            	$tracker_id = $input['tracker_id'];

            	$trackerFound = TrackerModel::searchTracker($tracker_id);
            	
            	if (sizeof($trackerFound) > 0) {
            		$from_point = $input['from_point'];
	                $to_point = $input['to_point'];
	                $cost = $input['cost'];
	                $time = $input['time'];

	                $isFound = RoutesModel::searchRoute(NULL,$from_point,$to_point);
	                
	                if (sizeof($isFound) > 0) {
	                	$isInserted = false;
	                	$error['errors'] = 'Points '.$from_point .' To ' .$to_point .' is Prohibited';
	                } else {

	                	$isFound = RoutesModel::searchRoute(NULL,NULL,$to_point,$tracker_id);
	                	
	                	if (sizeof($isFound) > 0) {
	                		$isInserted = false;
	                		$error['errors'] = 'Multiple Destination is not allowed';
	                	} else {
	                		$isInserted = RoutesModel::createRoutes($tracker_id,$from_point,$to_point,$cost,$time);
	            			$isInserted = true;
	                	}
	                	
	                }
            	} else {
            		$isInserted = false;
            		$error['errors'] = 'Tracker Id Does not Exist';
            	}
                
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,201);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }

    public function update(Request $request,$id) {
    	$response = '';
    	$result = '';
    	$errors = [];

        try {

    		$fields = $request->only('tracker_id','from_point','to_point','cost','time');

    		$isFound = RoutesModel::searchRoute($id);

    		if (sizeof($isFound) > 0 ) {

	        	if (isset($fields['to_point'])) {
	        		$tracker_id = $fields['tracker_id'];
		        	$isFoundRoute = RoutesModel::searchRoute(NULL,NULL,NULL,$tracker_id);
			    	$data = json_decode($isFoundRoute,true);
			    	
			    	$deleteRoute = [];

			        foreach ($data as $key => $value) {
			            if(!in_array($value['from_point'],$deleteRoute)) {
			                array_push($deleteRoute,$value['from_point']);
			            }

			            if(!in_array($value['to_point'],$deleteRoute)) {
			                array_push($deleteRoute,$value['to_point']);
			            }
			        }

			    	$deleteKey = array_keys($deleteRoute,$fields['to_point']);
			    	$size = sizeof($deleteRoute);

			    	for ($i=$deleteKey[0]; $i <= $size; $i++) { 
			    		unset($deleteRoute[$i]);
			    	}

			    	array_shift($deleteRoute);

			    	$deactivate = RoutesModel::deleteUnsedRoute($deleteRoute,$tracker_id);
	        	} else if ((isset($fields['to_point'])) && (isset($fields['from_point']))) {
	        		$isFound = RoutesModel::searchRoute(NULL,$fields['from_point'],$fields['to_point']);

	                if (sizeof($isFound) > 0) {
	                	$errors = 'Points '.$fields['from_point'] .' To ' .$fields['to_point'] .' is Prohibited';
	                } else {
						$result = RoutesModel::updateRoute($id,$fields);	
	                }
	        	} else {
	        		$result = RoutesModel::updateRoute($id,$fields);
	        	}

		    	
		    	
    		} else {
    			$errors = 'Not Found';
    		}

        } catch(Exception $error) {

        } finally {

        	if ($result) {
        		$response = response()->json(array('message' => 'Route Updated'),200);
        	} else {
        		$response = response()->json(array('message' => $errors),500);
        	}

            return $response;
        }

    }

    public function delete($id) {
    	$response = '';
    	$result = '';

        try {
        	
        	$isFound = RoutesModel::getConnectedRoutes($id);
        	$dataRoutes = json_decode($isFound,true);
        	$error = '';

        	if (sizeof($isFound) >= 2) {

        		$error ='This routes are connected to ';

        		foreach ($dataRoutes as $key => $value) {
        			if ($key === 0)
        				$error .= $dataRoutes[$key]['from_point'].' and '.$dataRoutes[$key]['to_point'];
        			else 
        				$error .= ' and '.$dataRoutes[$key]['from_point'].' and '.$dataRoutes[$key]['to_point'];
        		}

        		$response = response()->json(array('message' => 'Routes cannot be deleted','reason' => $error),200);

        	} else if (sizeof($isFound) === 0){
        		
	        	 $result = RoutesModel::deleteRoute($id);	
	      
        	} else {

        		if (sizeof($isFound) > 0) {
	        		$result = RoutesModel::deleteRoute($id);	
	        	} else {
	        		$response = response()->json(array('error' => 'Not Found'),404);
	        	}

        	}

        } catch(Exception $error) {

        } finally {

        	if ($result) {
        		$response = response()->json(array('message' => 'Route Deleted'),200);
        	}

            return $response;
        }

    }

}
