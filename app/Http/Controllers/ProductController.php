<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ProductModel;

class ProductController extends Controller
{
	public function checkWarehouse($id) {
    	$bool = '';
    	try {
    		$isFound = ProductModel::checkWarehouse($id);
    		if (sizeof($isFound) > 0) {
    			$bool = true;
    		} else {
    			$bool = false;
    		}
    	} catch (Exception $e) {
    		
    	} finally {
    		return $bool;
    	}
    }

    public function insert(Request $request) {
    	$isInserted = false;
        $error['errors'] = [];
        try {
            $input = $request->only('warehouse_id','name','description','qty','price');

            $rules = array(
                           'description'=>['required','regex:/^[a-zA-Z0-9]/'],
                           'name'=>['required','regex:/^[a-zA-Z0-9]/'],
                           'price'=>['required','regex:/^[0-9]+$/'],
                           'qty'=>['required','regex:/^[0-9]+$/'],
                           'warehouse_id'=>['required','regex:/^[0-9]+$/']
                          );

            $validate = Validator::make($input,$rules);
             
            if ($validate->fails()) {
                $isInserted = false;
                array_push($error['errors'],json_decode($validate->errors(),true));
            } else {

                $warehouse_id = $input['warehouse_id'];
                $name = $input['name'];
                $description = $input['description'];
                $qty = $input['qty'];
                $price = $input['price'];

                if($this->checkWarehouse($warehouse_id)) {
                	$isInserted = ProductModel::createProduct($warehouse_id,$name,$description,$qty,$price);
                } else {
                	$error['errors'] = 'Warehouse not found';
                }
                
                //$isInserted = true;
            }
        } catch(Exception $error) {

        } finally {
            $json = '';
            if ($isInserted) {
                $json = array('errors'=>[],
                              'status' => 'ok',
                              'code' => 200,
                              'msg' => 'data inserted'
                              );
                $responseJSON = response()->json($json,201);
            } else {
                $responseJSON = response()->json($error,200);
            }
            return $responseJSON;
        }
    }
}
