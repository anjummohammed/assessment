<?php

namespace App\Http\Middleware;

use Closure;
use App\TokenModel;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check if headers has Authorization and belongs to user
        $auth_token = explode(' ',$request->header('Authorization'));
        if (empty($auth_token[1])) {
            return response()->json(array('status'=>"Unauthorized Access"),401);
        } else {
            $isFound = TokenModel::getToken($auth_token[1]);
            $token = json_decode($isFound,true);
            if ((sizeof($token) > 0) && ($token[0]['token'] === $auth_token[1]) && ($token[0]['role_id'] === 1)) {
                return $next($request);
            } else {
                return response()->json(array('status'=>"Unauthorized Access"),401);
            }
        }
    }
}
