<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoutesModel extends Model
{
    public static function createRoutes($tracker_id,$from_point,$to_point,$cost,$time) {
    	$sql = '';
    	try {
			$sql = DB::table('routes')->insert([
			    [
			     'tracker_id' => $tracker_id,
			     'from_point' => $from_point,
			     'to_point' => $to_point,
			     'time' => $time,
			     'cost' => $cost
			    ]
			]);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function searchRoute($id = null , $origin = null , $destination = null ,$tracker_id = null) {
    	$sql = '';
    	try {
    		if (!is_null($id)) {
    			$sql = DB::table('routes')
    			  ->select('tracker_id','from_point','to_point','cost','time')
    			  ->where('id','=',$id)
    			  ->get();
    		} else if ((!is_null($destination)) && (!is_null($origin))) {
    			$sql = DB::table('trackers')
    			  ->select('id')
    			  ->where('destination','=',$destination)
    			  ->where('origin','=',$origin)
    			  ->get();
    		} else if ((!is_null($destination)) && (!is_null($tracker_id))) {
    			$sql = DB::table('routes')
    			  ->select('tracker_id')
    			  ->where('to_point','=',$destination)
    			  ->where('tracker_id','=',$tracker_id)
    			  ->get();
    		} else if(!is_null($tracker_id)) {
    			$sql = DB::table('routes')
    			  ->select('tracker_id','from_point','to_point','cost','time')
    			  ->where('tracker_id','=',$tracker_id)
    			  ->get();
    		}
			
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function getConnectedRoutes($id){
    	$sql = '';
    	try {
			$sql = DB::table('routes')
				  ->select('id','tracker_id','from_point','to_point','cost','time')
				  ->where('tracker_id',function ($query) use ($id) {
	            	$query->select('tracker_id')->from('routes')
	            	->where('id','=',$id);})
				  ->where(function($query) use ($id){
				  	$query->orWhere('to_point',function ($query2) use ($id) {
			                	$query2->select('from_point')->from('routes')
			                	->where('id','=',$id);
			                   })
				  	        ->orWhere('from_point',function ($query3) use ($id) {
			                	$query3->select('to_point')->from('routes')
			                	->where('id','=',$id);
			                   });
				   })->get();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function updateRoute($id,$fields) {
    	$sql = '';
    	try {
			$sql = DB::table('routes')
    			  ->where('id',$id)
    			  ->update($fields);
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function deleteRoute($id) {
    	$sql = '';
    	try {
  			$sql = DB::table('routes')
    			  ->where('id',$id)
    			  ->delete();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function getAllRoutes() {
    	$sql = '';
    	try {
			$sql = DB::table('routes')
    			  ->select('tracker_id','from_point','to_point','cost','time')
    			  ->get();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }

    public static function deleteUnsedRoute($id,$tracker_id) {
    	$sql = '';
    	try {
  			$sql = DB::table('routes')
  				  ->where('tracker_id','=',$tracker_id)
    			  ->whereIn('from_point',$id)
    			  ->delete();
    	} catch (Exception $e) {

    	} finally {
    		return $sql;
    	}
    }
}
