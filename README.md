CRUD API OF DELIVERY SERVICE

setup your laravel environment 
1st install composer
2nd setup your database connection by creating .env file

system steps on crud
1st you can pick the token of admin located at tokens table to add,update,delete tracker or routes
2nd add tracker and specify your start route that the product will move
3rd add routes for the start route that you have created base on tracker id


Url /api/addTracker 

headers:Basic <token of the user>
method:POST
parameters:start_route,origin,destination

Url /api/updateTracker /{id of tracker}

headers:Basic <token of the user>
method:PUT
parameters:start_route,origin,destination

Url /api/deleteTracker /{id of tracker}

headers:Basic <token of the user>
method:DELETE

Url /api/addRoute 

headers:Basic <token of the user>
method:POST
parameters:tracker_id,from_point,to_point,cost,time

Url /api/updateRoute/{id of route}

headers:Basic <token of the user>
method:PUT
body:x-www-form-urlencoded
parameters:tracker_id,from_point,to_point,cost,time(other fields than this is not accepted)

Url /api/deleteRoute/{id of route}

headers:Basic <token of the user>
method:DELETE

Url /api/deleteTracker/{id of tracker}

headers:Basic <token of the user>
method:DELETE

Url /api/searchRoutes

headers:Basic <token of the user>
method:POST
parameters:origin,destination

Tables: 
users
tokens
routes
trackers
user_details
roles

sql dump:
/public/dump/assessment.sql



